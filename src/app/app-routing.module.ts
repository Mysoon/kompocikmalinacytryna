import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BooksComponent } from "./employees/books.component";

const routes: Routes = [
  {
    path: "books",
    component: BooksComponent,
    data: { title: "List of Books" }
  },

  { path: "", redirectTo: "/books", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}