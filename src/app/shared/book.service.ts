import { AngularFirestore } from "@angular/fire/firestore";
import { Injectable } from "@angular/core";
import { Book } from "./book.model";

@Injectable({
  providedIn: "root"
})
export class BookService {
  formData: Book;

  constructor(private firestore: AngularFirestore) {}

  getBooks() {
    return this.firestore.collection("books").snapshotChanges();
  }
  deleteBook(id: string) {
    this.firestore.doc("books/" + id).delete();
  }

  addBook(book: Book) {
    this.firestore.collection("books").add(book);
  }

  udpateBook(book: Book, id: string) {
    this.firestore.doc("books/" + id).update(book);
  }
}
