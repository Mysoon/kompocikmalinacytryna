import { Component, OnInit, Input } from "@angular/core";
import { BookService } from "src/app/shared/book.service";
import { NgForm } from "@angular/forms";
import { AngularFirestore } from "@angular/fire/firestore";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-book",
  templateUrl: "./book.component.html",
  styleUrls: ["./book.component.css"]
})
export class BookComponent implements OnInit {
  constructor(
    private service: BookService,
    private firestore: AngularFirestore,
    private toastr: ToastrService
  ) {}
  @Input() massage: string;

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) form.resetForm();
    this.service.formData = {
      id: null,
      title: "",
      author: "",
      description: "",
      ISBN: ""
    };
  }

  onSubmit(form: NgForm) {
    let book = Object.assign({}, form.value);
    delete book.id;
    let id = form.value.id;
    if (form.value.id == null) this.service.addBook(book);
    else this.service.udpateBook(book, id);
    this.resetForm(form);
  }
}
