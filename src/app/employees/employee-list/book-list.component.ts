import { AngularFirestore } from "@angular/fire/firestore";
import {
  Component,
  OnInit,
  EventEmitter,
  Input,
  Output,
  SystemJsNgModuleLoader
} from "@angular/core";
import { BookService } from "src/app/shared/book.service";
import { Book } from "src/app/shared/book.model";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-book-list",
  templateUrl: "./book-list.component.html",
  styleUrls: ["./book-list.component.css"]
})
export class BookListComponent implements OnInit {
  list: Book[];

  constructor(
    private service: BookService,
    private firestore: AngularFirestore,
    private toastr: ToastrService
  ) {}
  @Output() public childEvent = new EventEmitter();
  ngOnInit() {
    this.service.getBooks().subscribe(actionArray => {
      this.list = actionArray.map(item => {
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as Book;
      });
      this.childEvent.emit(this.list.length);
    });
  }

  onEdit(book: Book) {
    this.service.formData = Object.assign({}, book);
  }

  onDelete(id: string) {
    this.service.deleteBook(id);
  }
}
